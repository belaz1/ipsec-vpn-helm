# ipsec-vpn-server

![Version: 0.0.26](https://img.shields.io/badge/Version-0.0.26-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: latest](https://img.shields.io/badge/AppVersion-latest-informational?style=flat-square)

## DISCLAIMER

This chart is experimental.

It creates a VPN server using the IPsec protocol.

For now, it only supports pre-shared keys (PSK) out of the box.

Allows multi-entities connections.

## Installing the Chart

```console
$ helm repo add ipsec-vpn https://gitlab.com/api/v4/projects/43498617/packages/helm/stable
$ helm install my-ipsec-vpn-server ipsec-vpn/ipsec-vpn-server --version 0.0.26
```

Deploy IPsec VPN server inside K8s with optional sealed-secrets

## Maintainers

| Name | Email | Url |
| ---- | ------ | --- |
| Belazar Mohamed | <l3v3l_belaz@hotmail.com> | <https://gitlab.com/belaz1> |

## Source Code

* <https://gitlab.com/belaz1/ipsec-vpn-helm>

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` |  |
| autoscaling.enabled | bool | `false` |  |
| autoscaling.maxReplicas | int | `100` |  |
| autoscaling.minReplicas | int | `1` |  |
| autoscaling.targetCPUUtilizationPercentage | int | `80` |  |
| fullnameOverride | string | `"vpn"` |  |
| image.pullPolicy | string | `"Always"` |  |
| image.repository | string | `"aeron/ikev2-strongswan-vpn"` | Strongswan vpn server container |
| image.tag | string | `"23.0"` |  |
| imagePullSecrets | list | `[]` |  |
| ipsec_secrets.config | string | `": PSK \"SECRET_PSK\"\n"` |  |
| nameOverride | string | `""` |  |
| nodeSelector | object | `{}` |  |
| podAnnotations | object | `{}` |  |
| podSecurityContext | object | `{}` |  |
| replicaCount | int | `1` |  |
| resources | object | `{}` |  |
| sealed_secrets | bool | `false` |  |
| securityContext | object | `{}` |  |
| service.annotations | object | `{}` |  |
| service.ports.ike.name | string | `"vpn-ike"` |  |
| service.ports.ike.port | int | `4500` |  |
| service.ports.isakmp.name | string | `"vpn-isakmp"` |  |
| service.ports.isakmp.port | int | `500` |  |
| service.type | string | `"ClusterIP"` |  |
| serviceAccount.annotations | object | `{}` |  |
| serviceAccount.create | bool | `false` |  |
| serviceAccount.name | string | `""` |  |
| tolerations | list | `[]` |  |
| vpnConfigs[0].auth | string | `"psk"` |  |
| vpnConfigs[0].ike_version | int | `2` |  |
| vpnConfigs[0].local_id | string | `"test.test.com"` |  |
| vpnConfigs[0].local_ts | string | `"0.0.0.0/0"` | Local ip range |
| vpnConfigs[0].name | string | `"vpn-test"` |  |
| vpnConfigs[0].proposals | string | `"aes256-sha256-ecp384,aes128-sha512-modp2048,aes256-sha512-modp2048,aes128-sha1-modp2048"` |  |
| vpnConfigs[0].remote_addrs | string | `"%any"` |  |
| vpnConfigs[0].remote_id | string | `"%any"` |  |
| vpnConfigs[0].remote_ts | string | `"192.168.1.0/24,10.0.0.0/8,192.168.0.0/16,172.16.0.0/12"` | Remote ip range |